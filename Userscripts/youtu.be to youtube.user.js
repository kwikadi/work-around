// ==UserScript==
// @name           youtu.be to youtube
// @namespace      Example
// @description    Expand youtube links
// @include        *
// ==/UserScript==

var links = document.getElementsByTagName("a");
var regex = /^(https?:\/\/)([^.]?\.?)(youtu\.be\/)(.+)$/i;
for (var i=0,imax=links.length; i<imax; i++) {
   links[i].href = links[i].href.replace(regex,'$1$2youtube.com/watch?v=$4');
    //specific to reddit
    links[i].setAttribute('data-outbound-url', links[i].href);
}