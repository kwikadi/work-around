// ==UserScript==
// @name           gifv to gif
// @namespace      Example
// @description    Replace gifv links to gif
// @include        *
// ==/UserScript==

var links = document.getElementsByTagName("a");
var regex = /^(.+)(\.gifv)$/i;
for (var i=0,imax=links.length; i<imax; i++) {
   links[i].href = links[i].href.replace(regex,'$1.gif');
    //specific to reddit
    links[i].setAttribute('data-outbound-url', links[i].href);
}