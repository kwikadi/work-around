// ==UserScript==
// @name           Imgur to Filmot
// @namespace      Example
// @description    Redirect imgur links to filmot
// @include        *
// ==/UserScript==

var links = document.getElementsByTagName("a");
var regex = /^(https?:\/\/)([^.]?\.?)(imgur\.com\/)(.+)$/i;
for (var i=0,imax=links.length; i<imax; i++) {
   links[i].href = links[i].href.replace(regex,'http://$2filmot.org/$4');
    //specific to reddit
    links[i].setAttribute('data-outbound-url', links[i].href);
}