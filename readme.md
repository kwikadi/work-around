# work-around

So work has some really bizarre policies. It allows reddit but no imgur, youtube but blocks youtu.be, has access to facebook and all that jazz but every now and then, I stumble upon mostly harmless sites which are blocked. I actually asked IT what was up with that, and their response was "viruses". Since they won't budge, I figured I had to do something in order to get around this nonsense. And then, I did.


## Userscript

### disable reddit tracking

This seems like a bizarre addition, until you realise that I dont stay logged in on my reddit on my work laptop, and their tracking absolutely fucks up URLs. I had a simple chrome extention (called filmot) which would happily redirect imgur links to their proxy, but it doesnt work due to reddit's stupid tracking, and reddit doesnt allow for not tracking when you arent logged in (They do, to their credit, allow stopping all tracking when you are logged in).

### imgur to filmot

I just mentioned that a filmot extension already did this, so why did I need to make this user script? Well, 1. because I was interested in userscripts and wanted to write one, and also because I don't want to rely on filmot and end up nowhere if/when filmot shuts down or something. If filmot ever does shut shop, I'll just setup a proxy for imgur on my own server too, and it'll be a single line change before I'm up and running again.

### gifv to gif

filmot does not handle gifv well on the work network (it works fine at home). Since I dont use this pos laptop at home anyway, and imgur is kind enough to give a gif if you just change the URL, I have a userscript for this.

### youtu.be to youtube

Stupid work network blocks youtu.be but not youtube. Yeah, let that sink in for a bit.

## Proxy

Basically the script that will run on my server so I can sail on the vast seas of the internet unfettered by work's policies. Gfycat redirect right now, with 9gag, urbandictionary and others to come soon. Note that this will require a userscript too.